variable "yc_account_key_file" {
    description = "Service account key file"
    default = "~/key.json"
}

variable "yc_cloud_id" {
    description = "cloud_id"
    default = "b1ghu41###########"
}

variable "yc_folder_id" {
    description = "folder_id"
    default = "b1ghu41###########"
}

variable "yc_zone" {
    description = "zone"
    default = "ru-central1-a"
}

variable "name_backend_sa_user" {
    description = "name_backend_sa_user"
    default = "terraform-backend-sa-user"
}

variable "name_backend" {
    description = "name_backend"
    default = "buckend-s3-test-k8s"
}