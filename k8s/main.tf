terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  backend "s3" {
  }
  required_version = ">= 0.13"
}
provider "yandex" {
  service_account_key_file = var.yc_account_key_file
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_zone
}
resource "yandex_kubernetes_cluster" "lab-k8s" {
  name = var.yc_kubernetes_cluster_name
  description = "Deusops lab install OpenCart"
  network_id = yandex_vpc_network.k8s-network.id
  
  master {
    zonal {
      zone      = yandex_vpc_subnet.k8s-subnet-1.zone
      subnet_id = yandex_vpc_subnet.k8s-subnet-1.id
    }
    version   = var.yc_kubernetes_cluster_version
    public_ip = var.yc_kubernetes_cluster_public_ip
  } 
  service_account_id      = yandex_iam_service_account.k8s-master.id
  node_service_account_id = yandex_iam_service_account.k8s-master.id
    depends_on = [
      yandex_resourcemanager_folder_iam_binding.editor,
      yandex_resourcemanager_folder_iam_binding.images-puller
    ]
}

resource "yandex_vpc_network" "k8s-network" {
  name = "k8s-network" 
}

resource "yandex_vpc_subnet" "k8s-subnet-1" {
  v4_cidr_blocks = ["192.168.10.0/24"]
  zone           = var.yc_zone
  network_id     = yandex_vpc_network.k8s-network.id
}

resource "yandex_iam_service_account" "k8s-master" {
  name        = "k8s-master"
  description = "k8s-master user"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  # Сервисному аккаунту назначается роль "editor".
  folder_id = var.yc_folder_id
  role      = "editor"
  members   = [
    "serviceAccount:${yandex_iam_service_account.k8s-master.id}"
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
  # Сервисному аккаунту назначается роль "container-registry.images.puller".
  folder_id = var.yc_folder_id
  role      = "container-registry.images.puller"
  members   = [
    "serviceAccount:${yandex_iam_service_account.k8s-master.id}"
  ]
}

resource "yandex_kubernetes_node_group" "k8s_group" {
  cluster_id  = "${yandex_kubernetes_cluster.lab-k8s.id}"
  name        = "k8s-node"
  description = "Kubernetes node"
  version     = var.yc_kubernetes_cluster_version
  instance_template {
    platform_id = "standard-v2"
    network_interface {
      nat                = true
      subnet_ids         = ["${yandex_vpc_subnet.k8s-subnet-1.id}"]
    }
    resources {
      memory = 2
      cores  = 2
    }
    boot_disk {
      type = "network-hdd"
      size = 64
    }
    scheduling_policy {
      preemptible = false
    }
    container_runtime {
      type = "containerd"
    }
  }
  scale_policy {
    fixed_scale {
      size = 1
    }
  }
  allocation_policy {
    location {
      zone = var.yc_zone
    }
  }
}

#data "yandex_kubernetes_cluster" "my_cluster" {
#  cluster_id = "${yandex_kubernetes_cluster.lab-k8s.id}"
#}
#
#output "cluster_external_v4_endpoint" {
#  value = "${data.yandex_kubernetes_cluster.my_cluster.master.0.external_v4_endpoint}"
#}

resource "null_resource" "command_connekt_yc_kubectl" {
  provisioner "local-exec" {
    command = "yc managed-kubernetes cluster get-credentials --id ${yandex_kubernetes_cluster.lab-k8s.id} --external --force"
    
  }
  
}