variable "yc_account_key_file" {
    description = "Service account key file"
    default = "~/key.json"
}

variable "yc_cloud_id" {
    description = "cloud_id"
    default = "b1ghu41###########"
}

variable "yc_folder_id" {
    description = "folder_id"
    default = "b1ghu41###########"
}

variable "yc_zone" {
    description = "zone"
    default = "ru-central1-a"
}



