terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  backend "s3" {
  }
  required_version = ">= 0.13"
}
provider "yandex" {
  service_account_key_file = var.yc_account_key_file
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_zone
}

data "terraform_remote_state" "k8s" {
  backend = "s3"
  config = {
    endpoint   = var.yc_backend_k8s_endpoint
    bucket     = var.yc_backend_k8s_bucket_name
    region     = var.yc_zone
    key        = var.yc_backend_k8s_key
    access_key = var.yc_backend_k8s_access_key
    secret_key = var.yc_backend_k8s_secret_key

    skip_region_validation      = true
    skip_credentials_validation = true
  }
  
}

resource "yandex_mdb_postgresql_cluster" "mypg" {
  name                = "mypg"
  environment         = "PRESTABLE"
  network_id          = data.terraform_remote_state.k8s.outputs.k8s-network-id
  security_group_ids  = [ yandex_vpc_security_group.pgsql-sg.id ]
#  deletion_protection = true

  config {
    version = 14
    resources {
      resource_preset_id = "s2.micro"
      disk_type_id       = "network-ssd"
      disk_size          = "20"
    }
  }

  host {
    zone      = var.yc_zone
    name      = "mypg-host-a"
    subnet_id = data.terraform_remote_state.k8s.outputs.k8s-subnet-1-id
  }
}

resource "yandex_mdb_postgresql_database" "db1" {
  cluster_id = yandex_mdb_postgresql_cluster.mypg.id
  name       = "db1"
  owner      = "user1"
}

resource "yandex_mdb_postgresql_user" "user1" {
  cluster_id = yandex_mdb_postgresql_cluster.mypg.id
  name       = "user1"
  password   = "user1user1"
}

#resource "yandex_vpc_network" "mynet" {
#  name = "mynet"
#}

#resource "yandex_vpc_subnet" "mysubnet" {
#  name           = "mysubnet"
#  zone           = var.yc_zone
#  network_id     = yandex_vpc_network.mynet.id
#  v4_cidr_blocks = ["192.168.11.0/24"]
#}

resource "yandex_vpc_security_group" "pgsql-sg" {
  name       = "pgsql-sg"
  network_id = data.terraform_remote_state.k8s.outputs.k8s-network-id

  ingress {
    description    = "PostgreSQL"
    port           = 6432
    protocol       = "TCP"
    v4_cidr_blocks = [ "0.0.0.0/0" ]
  }
}